# a bite of haidilao

This a mobile app for haidilao in the 2014 baidu hackthon

It is a light app

# Main

![Main](https://github.com/wcc526/haidilao/raw/master/docs/sceenshots/1.png)

# Smart Order

![Order](https://github.com/wcc526/haidilao/raw/master/docs/sceenshots/2.png)

# Special chafing dish

![Special](https://github.com/wcc526/haidilao/raw/master/docs/sceenshots/3.png)

# Interesting game

![Game](https://github.com/wcc526/haidilao/raw/master/docs/sceenshots/4.png)

# Winner bonus

![Winner](https://github.com/wcc526/haidilao/raw/master/docs/sceenshots/5.png)

