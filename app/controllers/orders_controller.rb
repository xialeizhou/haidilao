class OrdersController < ApplicationController
  def new
    @order=Order.new
  end
  def done
  end
  def create
    @order=Order.new(order_params)
    if @order.save
      redirect_to "/done"
    end
  end

  private
    def order_params
      params.permit(:phone,:people)
    end
end
