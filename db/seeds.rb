# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Store.create(name:"万柳店",  smallpic:"1.jpg",address:"北京市海淀区巴沟路2号北京华联万柳购物中心5层")
Store.create(name:"大钟寺店",smallpic:"2.jpg",address:"北京海淀区北三环西路23号大钟寺中坤广场E栋5楼")
Store.create(name:"紫竹桥店",smallpic:"3.jpg",address:"北京海淀区紫竹院路北洼路4号1区195号楼苏宁电器4楼")
Store.create(name:"牡丹园店",smallpic:"4.jpg",address:"海淀区花园东路2号牡丹宾馆北200米")
Store.create(name:"西直门店",smallpic:"5.jpg",address:"北京西城区西直门北大街32号枫蓝国际购物中心3楼")
Store.create(name:"翠微路店",smallpic:"6.jpg",address:"北京市海淀区翠微路5号嘉茂购物中心4楼")
Store.create(name:"大屯路店",smallpic:"7.jpg",address:"北京市朝阳区大屯路北路312号金泉美食宫4楼")

Pot.create(name:"番茄火锅",pic:"1.jpg",feature:"番茄火锅的味道清香，可以涮食海鲜、动物肉品、各类蔬菜。在涮锅前还可以品尝涮锅的汤料。番茄内富含人体所需的多种维生素，尤其是维C较多，维C有助于对人体的消化及减缓皮肤衰老",suit:"适合老人、女士、儿童和醉酒的人士食用")
Pot.create(name:"三鲜火锅",pic:"2.jpg",feature:"选用白味底料、枸杞、大枣、香菇等符合国家食品安全的滋补食材，用骨汤提取物调配而成。口感鲜、香、浓、厚，有滋补养颜的功效。含滋补食材和上等骨汤，具有滋补健身、美容养颜的功效。",suit:"一般人群均可食用")
Pot.create(name:"菌汤火锅",pic:"3.jpg",feature:"富含优质蛋白质，低脂肪，含亚油酸，亚麻酸，保健多糖，丰富维生素和矿物质；具有增强抵抗力、防辐射、抗衰老、调节内分泌、保肝护肝、镇静安神、润肺祛痰的功效。",suit:"一般人群均可食用")
Pot.create(name:"香辣河鲜火锅",pic:"4.jpg",feature:"富含优质蛋白质、不饱和脂肪酸、维生素A、维生素D、多种矿物质(如钙、磷、钾、铁、碘、硒等)；具有滋补健身、保持身材、降血压、降血脂等功效。",suit:"喜好吃辣的客人")
Pot.create(name:"蹄花火锅",pic:"5.jpg",feature:"选用白味底料，配以猪蹄、冬瓜、枸杞、大枣等，美味滋补，营养丰富。富含胶原蛋白，具有滋补养颜的功效",suit:"女性，孕产妇，体质虚弱的人")
Pot.create(name:"鸳鸯火锅",pic:"6.jpg",feature:"含滋补食材和上等骨汤，具有滋补健身、美容养颜的功效。选用一次性清油底料，口感醇和，辣而不燥，是当前比较营养健康的红汤火锅。",suit:"一般人群均可食用")
Pot.create(name:"无渣火锅",pic:"7.jpg",feature:"由清油火锅底料添加特殊辅料炒制后提炼而成，口感清爽，辣而不燥，让客人在吃火锅的时候更加的顺畅，（不再吃到夹在菜中的香料或辣椒皮）。",suit:"喜好吃辣的客人")

Level.create(question:"海底捞的创始人是谁?",a:"张勇",b:"张飞",c:"李勇",answer:"创始人张勇对员工绝对地信任，连服务员都有送菜、打折和免单权。",op:1,pic:"1.jpg")
Level.create(question:"海底捞成立时间是什么时候?",a:"1000",b:"1994",c:"2000",answer:"海底捞火锅成立于1994年，是一家以经营川味火锅为主，集各地火锅特色于一体的直营餐饮企业",op:2,pic:"2.jpg")
Level.create(question:"海底捞全中国有多少家门店?",a:"10",b:"50",c:"94",answer:"在全国有94家门店，连续5年获中国餐饮百强企业",op:3,pic:"3.jpg")
