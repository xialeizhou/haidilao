class CreatePots < ActiveRecord::Migration
  def change
    create_table :pots do |t|
      t.string :name
      t.string :pic
      t.string :feature
      t.string :suit

      t.timestamps
    end
  end
end
