class CreateLevels < ActiveRecord::Migration
  def change
    create_table :levels do |t|
      t.string :question
      t.string :pic
      t.string :a
      t.string :b
      t.string :c
      t.string :op
      t.string :answer

      t.timestamps
    end
  end
end
