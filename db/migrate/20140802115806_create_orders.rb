class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :people
      t.string :phone

      t.timestamps
    end
  end
end
